---
layout: markdown_page
title: "Considerations for transitioning a company to remote"
---

## On this page
{:.no_toc}

- TOC
{:toc}
## Introduction

On this page, we're detailing considerations for transitioning a colocated or hybrid-remote company to 100% remote, as well as tips for introducing remote team members to a historically in-office organization.

## How do you transition a hybrid team to a fully remote team?

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/MSj6-wC4f9w" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Darren (GitLab) and Janice (Groupdesk) discuss the potential of transitioning a hybrid-remote company to all-remote.*

In general, a company seeking to transition to fully remote should actively avoid or remove [common hybrid-remote pitfalls](/company/culture/all-remote/hybrid-remote/#disadvantages-to-hybrid-remote) by whatever means necessary. Moreover, it's important for leaders to put themselves in the shoes of a prospective job seeker who will ask [very specific questions](/company/culture/all-remote/evaluate/) to determine how prepared a company is to thrive in a 100% remote setting. 

Much of it boils down to changing whatever elements you need in order to become the remote company that you would want to work for. This requires old habits to be left behind, and intentional effort to be made on areas such as [documentation](/company/culture/all-remote/management/#scaling-by-documenting), [informal communication](/company/culture/all-remote/informal-communication/), and working well [asynchronously](/handbook/communication/).

If existing leadership is not equipped to understand these nuances or lead such a transformation, consider recruiting a Head of Remote with deep experience working in remote settings and leading globally distributed teams. [Andreas Klinger](https://twitter.com/andreasklinger/status/1187841403381878785) aptly articulates this role as being "at the intersection of optimizing internal tooling, processes, transparency, collaboration, efficiency, inclusivity, onboarding, hiring, employer-branding, culture and communication overall." 

The specific answer to this question will vary from company to company, and will be impacted by the following attributes.

1. Company size
1. Quantity of offices
1. Percentage of employees who already work remotely
1. Existing tools and technology infrastructure for operating remotely
1. Existing culture surrounding communication and career mapping

### Establish a remote infrastructure

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_HhlqwJsNyM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Darren (GitLab) and Anna-Karin (Because Mondays) discuss a number of challenges and solutions related to remote work, transitioning a company to remote, working asynchronously, and defaulting to documentation.*

Companies considering a transition to fully remote should ask themselves if they could function if every team member chose to work from their home tomorrow. To get an idea of what needs to be in place from a [tools and technology](/handbook/tools-and-tips/) standpoint, consider the following questions in light of the aforesaid scenario.

1. What voids would become apparent? 
1. What areas of communication would falter? 
1. What confusion would emerge? 
1. What tools do we use today that we would continue to use as a fully remote team?
1. What tools that empower fully remote teams should we consider?

Companies that rely on in-office meetings will likely need to embrace a new paradigm, leveraging Zoom and Google Calendar. Too, they should resist the urge to default to [meetings](/company/culture/all-remote/meetings/), relying instead on asynchronous communication wherever possible. 

Enabling access to systems and files across a fully remote company can be very different, and may require a new approach to security. 

### Document the culture

If you do not have a living, evolving [company handbook](/handbook/), start one now. Embracing the notion of "if it's not documented, it isn't actionable," is key to detaching from colocated norms and positioning for success with all-remote. Manage expectations that this handbook will never be complete, and it may take months or years to even feel comprehensive. 

Ideally, updating the handbook with policies and processes is something that the entire company is empowered and encouraged to do. For existing companies who do not have the luxury of starting their handbook as they start their business, consider hiring an arbiter to kickstart its creation and development. Former journalists and documentarians are great candidates for such a role. 

Consider each aspect of your company culture that is unwritten or implied, and document each one. In a fully remote setting, there are no daily in-person interactions where cues are absorbed. Hence, it is vital to over-communicate not just in daily work, but in explaining and detailing values that company culture is built upon. 

### Close the office

If at all possible, close the office. Shuttering an office (or multiple offices) offers the clearest signal that you're all-in with remote, and leaders are serious about ensuring that no employee is treated — consciously or otherwise — as a second-class citizen. 

### Equip and educate team members

While remote work is liberating and empowering, it can be [jarring and isolating](/company/culture/all-remote/drawbacks/) for those who are not equipped to manage the change. In an all-remote or remote-first company, team members are well aware of [what to expect](/company/culture/#life-at-gitlab) even before they apply for a role. In fact, many remote companies receive outsized interest in roles specifically *because* of their remote nature.

It's vital for leadership to understand that there may be resistance to a remote transition from employees who would prefer not to work remotely, or are anxious about the [lifestyle change](/company/culture/all-remote/people/). 

Clear and proactive internal communication is essential to removing fear and instilling excitement about the increased autonomy. 

1. Consider building an applicable guide to starting (or moving into) a remote role, akin to [GitLab's guide on the topic](/company/culture/all-remote/getting-started/). 
1. Host AMA (Ask Me Anything) sessions to get a better understanding of team member concerns and perspectives, and use that feedback to shape strategy. 
1. Retain workspace consultants to ensure that team members are equipped to work [comfortably and ergonomically in their home](/company/culture/all-remote/workspace).
1. Consider a policy that [reimburses fees associated with coworking spaces](/handbook/spending-company-money/), understanding that some employees will thrive in settings that mimic the office. 
1. Invest in education, including [conferences](/company/culture/all-remote/events/), courses, meetups, etc.
1. Formalize and document an [in-person strategy](/company/culture/all-remote/in-person/), embracing retreats, [company summits](/events/gitlab-contribute/), etc. to show that bonding in a physical space is still valued. 
1. Prepare for microgroups to form as people relocate. Leverage [geographic Slack channels](/handbook/communication/chat/#location-channels-loc_) to retain community even as people use newfound remote freedoms to move to a locale that is more personally fulfilling. 
1. Get intentional about [informal communication](/company/culture/all-remote/informal-communication/). With the removal of daily in-person interactions, you'll need to formalize mechanisms such as coffee chats, team social calls, and coworking calls to form and foster relationships. 

### Embrace iteration and transparency in transition

Expect bumps along the road. As with any significant business transformation, it's wise to communicate proactively to team members, customers, and investors that obstacles will emerge. Transitioning a company to fully remote, while still running the business, is not easy. It's a long-term bet that the short-term pain will be [worthwhile](/company/culture/all-remote/benefits/).

Leadership should be completely [transparent](/handbook/values/#transparency) with team members as the transition unfolds. Share headaches and roadblocks as well as successes. This should occur in an agreed channel so that discussion and feedback is centralized, and action items can be clearly disseminated. 

[Iteration](/handbook/values/#iteration) is a core value at GitLab, and we strive to [apply iteration to everything](/company/culture/all-remote/management/#applying-iteration-to-everything) — from [building a product](/blog/2019/04/26/agile-iteration-unique-onboarding-experience/) to shaping our [workspaces](/company/culture/all-remote/workspace/). This applies to business transformation as well. Implement the [smallest viable change](/handbook/values/#minimum-viable-change-mvc), solicit feedback, and tweak or revert if needed. By taking this approach to transforming your business, you're setting the example for how day-to-day tasks should be managed in a fully remote setting. 

## Adding remote employees to a primarily in-office culture

While GitLab believes that all-remote is the best structure to ensure that [our values are lived](/company/culture/all-remote/values/) and no team member is treated as a second-class citizen, the reality is that most companies formed to date have at least one physical office.  

In many instances, it is not feasible to transition entirely to all-remote, though leaders and recruiters are starting to realize that remote work is the future. Stripe, for example, has stated that its "[fifth engineering hub is remote](https://stripe.com/blog/remote-hub)."

> Stripe has engineering hubs in San Francisco, Seattle, Dublin, and Singapore. We are establishing a fifth hub that is less traditional but no less important: Remote. We are doing this to situate product development closer to our customers, improve our ability to tap the 99.74% of talented engineers living outside the metro areas of our first four hubs, and further our mission of increasing the GDP of the internet. — *David S., CTO at Stripe*

Jack Dorsey, CEO at Twitter, has shared that remote is the company's future.

<blockquote class="twitter-tweet tw-align-center"><p lang="en" dir="ltr">The <a href="https://twitter.com/hashtag/TweepTour?src=hash&amp;ref_src=twsrc%5Etfw">#TweepTour</a> is over! 27 offices visited around the world, our last visit being our future: remote. So many stories, so many amazing folks. Thank you all! <a href="https://twitter.com/hashtag/oneteam?src=hash&amp;ref_src=twsrc%5Etfw">#oneteam</a> ❤️</p>&mdash; jack 🌍🌏🌎 (@jack) <a href="https://twitter.com/jack/status/1189340061206306816?ref_src=twsrc%5Etfw">October 30, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
In instances where a company intends to maintain at least one physical office, but will allow remote hires for certain roles, added care must be taken. There is effectively nothing that can be done to completely remove bias that comes from seeing someone in the same physical space day after day, so leadership must be intentional about considering remote employees for input, feedback, promotion, and mentorship. 

Added care must be taken to ensure that the usual [downsides of hybrid-remote](/company/culture/all-remote/hybrid-remote/#disadvantages-to-hybrid-remote) are not tolerated. Conversely, employees considering a hybrid-remote role must be prepared to gracefully manage said challenges should they arise. 

Above all, a company which intends to begin hiring remotely (or allowing existing employees to optionally transition from colocated to remote) **must structure the company as if every single team member were remote**. 

This means that any hallway conversations must be documented and disseminated for all who were not present to hear, and it means that in-office workers must each use a single webcam and microphone to join a group call (as opposed to in-office colleagues [gathering in a conference room with a sole camera](/handbook/communication/#hybrid-calls-are-horrible)).

It means, for example, that colleagues who are sitting side-by-side must use a public Slack channel to communicate, as vocalizing a conversation would be unfair to team members who are remote. 

This requires tremendous effort, day after day, from every member of the company. 

## Contribute your lessons

Making remote work well, particularly in companies with colocated roots, is a shared challenge. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page. 

Return to the main [all-remote page](/company/culture/all-remote/).

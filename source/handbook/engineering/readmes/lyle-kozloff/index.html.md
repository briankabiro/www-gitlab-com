---
layout: markdown_page
title: "Lyle Kozloff's README"
job: "Support Engineering Manager"
---
### My Job
My job is to clear obstacles, set context, and build process. In general: to make your (work)
life easier, clearer and less stressful.

More, I have the privilege of helping you get better at things. I do this by helping you
set goals, providing feedback and acting as a voice of accountability.

### My Availability
Precious few things are more important than talking to you if you want to talk to me.
If you need to talk, let's talk. I'm here to enable your success - use me for that.

I snooze my Slack notifications when I'm not available. If you think of something at 2 a.m.
it's 100% okay to send me a message. I *expect* that you will also control your notifications.

Nothing on my calendar is urgent unless specifically noted as such. 


#### Other meetings
I'm open to ad-hoc meetings with anyone who wants to chat - whether you're in my department 
but report to someone else, or in a totally different department.

### My Assumptions

- **You're good at your job**
- **You know the team priorities and how they fit in with your personal priorities**
- **You will ask for help if you need it**
- **You feel safe pushing back**
- **We'll tell each other if there's something we could have done better**

### Greatest Strengths / Greatest Weakenesses
**I'm a collaborative leader and accept input readily.** At its best, this means
you'll feel empowered. At its worst it means that you'll feel like I don't have a
vision for what I want.

- **What to do if it's the worst**: Tell me that you're not clear on my expectations
and that my apparent indecision is blocking progress.

**I reprioritize constantly.** At its best, this means that important and impactful
things get done quickly. At its worst, it means that I miss a deliverable that
you were waiting on from me. 

 - **What to do if it's the worst**: Give me a firm deadline for when you need something
by. Check in on progress. I don't mind being managed up; if what I'm prioritizing
doesn't match your priorities I should be able to articulate what's blocking me.

**I operate my teams with a high degree of trust**: At its best, this means that
you are free to prioritize the tasks that you feel are the most impactful. At its
worst this means you may feel like I don't care what you're working on. 

 - **What to do if it's the worst**: Generally, I will only step in if you can't find 
a solution, you need my help or you've hit a blocker. Keep me informed, and be
clear if you're in one of the above states.

**I try and protect my teams from crappy work by doing it myself**: At its best, 
you'll be shielded from the small, annoying tasks that crop up. 
At its worst, I'll be drowning in these tasks and I'll ask you to do something 
that's (at best) half-done and probably overdue.

 - **What to do if it's the worst**: If I'm setting myself up as a blocker, call me
on it. I try and build process after I have a full understanding of the problem
set, but sometimes I need a nudge before I feel its ready to hand off. Sometimes
I misjudge the complexity or length of a task. 


### Your development
> *From the time you were very little, you've had people who have smiled you into 
> smiling, people who have talked you into talking, sung you into singing, loved
> you into loving.*
>
> *Fred Rogers*

One of the the best ways to learn is to have excellence modeled and to copy it.
Therefore:
- Continue pairing after your onboarding is over.
- Attend crush sesions; ask questions.
- Choose topics, seek out experts and relentlessly pursue them.

I want to help you grow. I can / will / have:
- play the role of your conscience and ask you annoying questions about your progress
- be a "study buddy" and grow/learn together
- help you find projects that require you to learn in a certain area
- cheer you on and broadcast your successes

#### Your development as a leader
One of the most important pieces of my job is to help you grow as a leader. Some of
you may be people leaders, some of you may lead as individual contributors - but
your ability to influence decisions will be a determining factor you in your 
immediate and future success. I will do my best to lend strength to your voice, 
and help your ideas find their way into results.

### Feedback
As with most managers, feedback is an area that I never feel like I give enough of.
If you're looking for feedback and I'm not offering it, it means I think
that you're doing just fine.

I do look for opportunities to compliment wins, but I miss things. If you're proud of 
something, please bring it up!

If you're not getting feedback and you want some, please ask for feedback in a *specific* area:
- "I'm not feeling confident about topic X, can you take a look at my work Y this week
and let me know how I did?"
- "This week I did X, even though we talked about doing Y, because of Z. Did I priotize correctly?"
- "Was this response clear?"
- "Should I be spending more time on X, or Y?"

This will help me give clear, timely feedback that will make a difference instead of a bumbling
"You're doing great!"

#### Behaviours I value:
- Initiative: you see a problem, formulate a solution and act on it, asking for input if only when necessary
- Humility: you do excellent work which draws attention to itself
- Consistency: you do what you say you'll do most (if not all) of the time
- Vulnerability: if you don't know, you ask (and in so doing, create an atmosphere where it's okay to not know and ask)

#### Feedback for me
Like you, I rely on feedback to improve. If you feel there is something I could do better,
please tell me! If I get salty, defensive or react negatively: call me on it.

If you don't feel comfortable addressing this with me directly, please tell my boss.

### 1<>1 Meetings
This is your meeting and your time: you set the agenda and determine what we talk about.
My most effective 1:1s have an agenda that builds up through the week, but I've had some
really great free-form ones as well. Don't feel bad if you don't come with something prepared.

If we don't have anything on the agenda, please expect me to try and get to know you better.

If you're struggling for topics, please include items that you've worked on this week. 

I like hearing about situations in your week because:
- I always learn something
- I get data points that help me make global process improvements
- I get a picture of what things you're great at
- I begin to understand which things you struggle with

**Don't save urgent matters for a 1-1**

### Other
- My expectations are sometimes expressed softly. If there is any doubt about whether
something I say is an expectation, a suggestion or an idea please ask.

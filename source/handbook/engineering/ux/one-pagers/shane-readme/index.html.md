---
layout: markdown_page
title: "Shane Bouchard's README"
---

This page was inspired by the recent trend of Engineering Manager READMEs. _e.g._ [Hackernoon: 12 Manager READMEs (from some of the best cultures in tech)](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe).


## Shane Bouchard’s README


My name is Shane Bouchard and I’m the [UX Manager](/job-families/engineering/ux-management/) of Plan, Create and Manage:

This README is intended to serve my fellow GitLabbers - a virtual introduction and invite, of sorts. My hope is that by getting to know me virtually you will be inspired to connect, collaborate and create with actual me 😎! 

* [GitLab Handle](https://gitlab.com/sbouchard1)  
* [Team Page](/company/team/#sbouchard1)  
* [My Website](https://www.uxshane.com/)

### About Me

* I’ve lived in [Orlando FL](https://goo.gl/maps/2YDcbbkBcPBNaaYV8) since the 1985. Yes, I’ve worked at Walt Disney World. Actually twice, in college as a cashier at Walt Disney World Studios and later as a UX Lead for WDW Parks & Resorts Technology. 
* I was originally born in [Racine, Wisconsin](https://goo.gl/maps/5TtgtQu6nCUafg2dA), which is between Milwaukee and Chicago on Lake Michigan. As a midwestern wisconsinite, I was taught the value of hard-work, the golden rule and cheese.
* In my free time, I’m an avid artist, outdoorsmen and traveller. I draw and paint in various styles, like realism, surrealism and cubism; backpack, camp and kayak; and travel as much as possible (10 or so different locations in the past two years).
* I’m an introverted extrovert. Yep, it is a thing. Introverted at home; extroverted at work (and on a dance floor). 
* I’m the father of two wonderful kids who mean everything to me. They like to roll their eyes when I pontificate about the magic of the 80’s.

### Working with me

* Put simply, I’m a design, marketing and product geek with 18+ years of experience in executive, management and lead roles.
* I’ve helped companies of all types and sizes - from big brands like Disney, Symantec and Microsoft to award-winning agencies and Lean SaaS Startups - build high-performing brands, cross-functional teams and omni-channel experiences. More on that on [LinkedIn](https://www.linkedin.com/in/bouchardshane) and my [personal website](https://www.uxshane.com/).
* I’m an Agile UX and Lean UX pro who lives to release valuable products at speed. In recent roles at Disney, Symantec and SharpSpring I served as an Agile and Lean strategist, coach and lead, working across disciplines and leadership tiers to build better products, faster. 
* I believe in the evidence-based innovation and design that Agile and Lean Startup promote, as I've seen time and again that teams can deliver more with less when they replace assumptions and bias with customer research and validation.
* I believe in teamwork, transparency and continual improvement. That none of us is as smart as all of us. That good ideas come from everywhere. And, there’s always room for improvement.
* In its simplest form, I believe design is about empathy and enablement. Or, if you prefer a one word description, alignment.
* I encourage designers to always be inclusive, strategic and pragmatic. To welcome feedback and harness the best ideas. To use Lean UX principles and practices to arrive at valuable, viable and validated solutions, quickly. To think like product professionals and focus on customer results, not pixels. 
* I can have strong opinions, but they’re softly held. I believe it’s not about me being right, it’s about it being right. I like it when others challenge my positions constructively, as that's how I learn and arrive at the best solutions.
* I’m persistent, but also patient. Determined, but also diplomatic. Analytical, but also creative. 

### Management style

* [Servant leader](https://en.wikipedia.org/wiki/Servant_leadership), who understands teams are made of uniquely gifted people with [different personalities](https://www.16personalities.com/personality-types) and ways to contribute. 
* As a manager I strive to be transparent, supportive and constructive. I address opportunities for improvement in a direct and productive way. 
* My favorite part of being a leader is the opportunity it provides to help companies, teams and individuals grow and achieve. Helping others, whether through design or management, is the most rewarding part of what I do.
* I'm open minded and pragmatic, which means I'll pivot to a better idea in a heartbeat. It also means I dont believe in [pulling rank](/handbook/values/#dont-pull-rank).

### Communication style

* I’m pro async, sync and everything between, as long as we’re communicating well. 
* I believe open communication is essential to building great teams and practices, and would rather over than under communicate.
* I’m available on all channels 9-5 EST, and sometimes check-in and work on nights and weekends.



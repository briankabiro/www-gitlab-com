---
layout: handbook-page-toc
title: "Offer Packages"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Offers

Before an offer is created, ensure the approval should go through [Greenhouse or BambooHR](/handbook/people-group/promotions-transfers/#offer-process-in-bamboohr-or-greenhouse).

### Offer Package in Greenhouse

#### Justification Section

This step is optional per each function.

Once it is determined that a candidate will be moving to the offer stage, the hiring manager will answer the following questions in the justification stage in the candidate's greenhouse profile:
- In what specific way(s) does this candidate make the team better?
- What flags were raised during the interview process?
- How do we intend on setting this candidate up for success?

The justification section is a good place to add as much detail and context as possible since offers will often be reviewed by individuals that were not part of the interview process so this is their primary source of information for whether or not an offer should be approved.

#### Recruiter Quality Check (Engineering Only)

Once a candidate has gone through the whole process before creating an offer the recruiter will ensure the following: 

- The candidate has been assessed on all the must have requirements for the role and passed
- The candidate has reached the bar set by the individual team i.e. a minimum of 5/9 nice to haves within Engineering
- The justification has sufficient detail and addresses any flags, no votes or not meeting the criteria as above 

The recruiter will leave a note in Greenhouse for the hiring manager as follows:

- Yes - Ready to move to offer approval 
- More info need - **suggestions to be added**

### Offer Package Creation

The hiring manager will work with the recruiter on the offer details and the recruiter will be responsible for submitting the official offer details through Greenhouse.

To create the offer package, move the candidate to the "Offer" stage in Greenhouse and select "Manage Offer." Input all required and relevant information, ensuring its correctness, and submit; then click `Request Approval`. **Please note that any changes in compensation packages will result in needing re-approval from each approver.**

#### Titles in the Offer Details

As we scale it is important to have some typical naming conventions when it comes to titles. Titles should follow this format: title, specialization, location. For example; Strategic Account Leader, DOD, Washington DC.

It is also important to recognize when a position is a manager of people compared to a manager of a product/function. If a manager of people the job title should be manager, department/division/specialization. For example; Manager, Recruiting. A product/function manager should be titled with Manager at the end of the title. For example; Marketing Program Manager. In some cases because of industry standards, this may not be possible; i.e. Area Sales Manager (ASM), should not be listed as Manager, Area Sales. 

#### Other Offer Details

   - Note that the offer package should include the candidate's proposed compensation in the most appropriate currency and format for their country of residence and job role. Annual and monthly salaries should be rounded up or down to the nearest whole currency unit and should always end with a zero (e.g., "50,110.00" or "23,500.00"). Hourly rates should be rounded to the nearest quarter-currency unit (e.g., 11.25/hr.).

   - For internal hires, be sure to include in the "Approval Notes" section the candidate's current level and position, as well as their compensation package.

   - You can also include any mitigating circumstances or other important details in the "Approval Notes" section of the offer details. If the comp has a variable component, please list base, on target earnings (OTE), and split in the "Approval Notes."

   - In case it is a public sector job family, please note (the lack of) clearances.

#### Counter Offer Details

Information in the offer package for counter offers should include the following in the "Approval Notes" section:

   - New offer:
   - Original offer:
   - Candidate's salary expectation beginning of process:
   - Candidate's counter offer:

#### Offer Approval Flow

1. The People Ops Analyst will receive an email notifying them of the offer.
   * The People Ops Analyst will ensure the compensation is in line with our compensation benchmarks.
   * Only one approval is needed in order to move forward.
   * If the hire is not in a low location factor area above 0.9, the e-group member responsible for the function and the CFO will be notified.
1. Next, The People Business Partners will receive a notification to approve.
1. Next, the executive of the division will then receive a notification to approve.

### Final Offer Approval

On offers at Director and above, approval is needed from the Chief People Officer (CPO) or the Chief Executive Officer (CEO), there is an `#offers` Slack channel where the requests should be added. The CEO or CPO should always be @mentioned for their approval. This Slack channel is private and only the recruiting team, CPO, CEO, and CFO have access to it. Please ensure your ping has:

1. Name
1. Position
1. GreenHouse profile link
1. For re-approvals clearly indicate what changed and why.

The CPO and CEO appreciate the thank you messages but they also have a hard time keeping up with Slack notifications. There is no need to say thanks, but if you do please add an emoji instead of sending a message.

If the offer is for a manager or individual contributor, the CPO or CEO do not need to approve.  However, please post in the '#offers' channel with "Offer has been extended for [Candidate Name] for [Position]" and a link to the candidate's Greenhouse profile. You may request the removal of the CPO/CEO from the offer approval section with a @ mention to a recruiting manager in GreenHouse or a ping in Slack.

### Communicating the Offer

Once the offer package has been approved by the approval chain, the verbal offer will be given, which will be followed by an official contract, which is sent through Greenhouse.

Offers made to new team members should be documented in Greenhouse through the notes thread between the person authorized to make the offer and the Candidate Experience Specialist.

   -  Greenhouse offer details should be updated as necessary to reflect any changes including start date. Sections updated that will trigger re-approval are noted in Greenhouse.

---
layout: handbook-page-toc
title: "Integrated Campaigns"
description: "GitLab Marketing Handbook: Integrated Campaigns"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

## Integrated Campaigns

The goal of GitLab integrated campaigns is to strategically land a cohesive message to a target audience across a variety of channels and offers dependent on the goals of the campaign. Content types include a combination of blog posts, webinars, whitepapers, reports, videos, case studies, and more. Channels include digital ads, social, paid social, SEO, PR, email, and more.  

**Marketing Program Managers** are responsible for the strategizing of integrated campaigns and organizing timelines and DRIs for deliverables to execute on the campaigns.

In an effort to continually improve our ability to derive success of campaigns, reporting is a focus using Bizible data in Periscope. [Issue here for more information on progress](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/982)

**Questions? Please feel free to ask in the [#marketing_programs slack channel](https://gitlab.slack.com/messages/CCWUCP4MS) or ask in the [#marketing slack channel](https://gitlab.slack.com/messages/C0AKZRSQ5) and tag @marketing-programs.**

## Active Integrated Campaigns

### 📌 Increase operational efficiencies

**MPM Owner: Jenny Tiemann**  

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/367) 
* [Live landing page](/just-commit/lower-tco/)
* [Campaign brief](https://docs.google.com/document/d/1H3pfX7nJk_LIF5tr_1ZnpPulyeCFT499Y3F8_qfWFPo/edit#heading=h.6jynaot9cbnq)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000000CyiL)

### 📌 Deliver better products faster

**MPM Owner: Zac Badgley**  

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/363) 
* [Live landing page](/just-commit/reduce-cycle-time/)
* [Campaign brief](https://docs.google.com/document/d/1dbEf1YVLPnSpFzSllRE6iNYB-ntjacENRMjUxCt8WFQ/edit)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000000Cyhm?srPos=0&srKp=701)

### 📌 Reduce security and compliance risk

**MPM Owner: Megan Mitchell**  

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/368) 
* [Live landing page](/just-commit/secure-apps/)
* [Campaign brief](https://docs.google.com/document/d/1NzFcUg-8c1eoZ1maHHQu9-ABFfQC65ptihx0Mlyd-64/edit)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000000CyeJ)
* 

### 📌 Operation OctoCat
**MPM Owner: Jackie Gragnola**  

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/439) 
* [Live landing page](/compare/github-actions-alternative/)
* [Campaign brief](https://docs.google.com/document/d/1Mcy_0cwMsTPIxWUXPgoqw9ejsRJxaZBHi4NikYTabDY/edit#heading=h.kf9lglu57c0t)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001lmdK)

### 📌 CI Build & Test Auto

**MPM Owner: Agnes Oetama**  

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/379) 
* [Live landing page](/compare/single-application-ci/)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001lkp9)

### 📌 Competitive campaign

**MPM Owner: Agnes Oetama**  

* [Parent epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/10)
* [Live landing page](/compare/single-application-ci/)
* [Campaign Brief >>](https://docs.google.com/document/d/1xtyv59PFUSkVSqOGj0oGpLYAppjV0-nUZiddIwjQPYw/edit?usp=sharing)
* [SFDC Campaign >>](https://gitlab.my.salesforce.com/70161000000VxvJ)
* [Meeting recordings >>](https://drive.google.com/drive/folders/1q5foxAX_Ezy6FGdLxKMXLwCpHkwxIRAA?usp=sharing)
* [Meeting slide deck >>](https://docs.google.com/presentation/d/1eaMBQjXZ8v5R8KGEB1H_DZ4hPpML8heP5X6lq8a3Q_U/edit#slide=id.g2823c3f9ca_0_30)

## Past Integrated Campaigns

### Just Commit

* [Parent epic with child epics and issues >>](https://gitlab.com/groups/gitlab-com/marketing/-/epics/7)
* [Former landing page (repurposed since close of the campaign)](/just-commit/)
* [SFDC Campaign](https://gitlab.my.salesforce.com/70161000000VwZq)
* [Meeting recordings >>](https://drive.google.com/drive/u/1/folders/147CtTEPz-fxa0m1bYxZUbOPBik-dkiYV)
* [Meeting slide deck >>](https://docs.google.com/presentation/d/1i2OzO13v77ACYo1g-_l3zV4NQ_46GX1z7CNWFsbEPrA/edit#slide=id.g153a2ed090_0_63)

## Campaign Planning

**There will be an epic for every campaign - created by the Marketing Program Manager managing the campaign - and a content pillar will align to the campaign, including gated assets to be created, pages to be created/revamped, blog posts, etc.**

* The campaign will have a clear launch date
* The new resources and webpages will have a clear due date prior to the launch date or defined for a cascading roll-out of content post-launch
* All action items will have DRIs and a timeline, working back from the launch date
* The campaign will be determined at least 45 days prior to launch to allow for a proactive, not reactive, timeline

Ideally, campaigns will be agreed at least a quarter in advance to allow for ample planning and execution, prep time with agencies, creative concepting, and communication internally. This is a collaborative effort to deliver a cohesive program.

#### Campaign Steps:
* **Campaign and launch date is determined by Marketing Leadership**
* **Assign:** Marketing Progragm Manager (MPM) is assigned
* **Assign:** Marketing team leaders assign DRIs from their teams
* **Meeting:** MPM organizes campaign kickoff call with early-stage DRIs (MPM, DMP, PMM, TMM)
* **Meeting:** MPM organizes campaign brief call with early-stage DRIs (MPM, DMP, PMM, TMM)
* **Plan:** MPM creates project plan (GANTT chart) with timelines and DRIs
* **Plan:** Marketing Program Manager creates the epic and related issues, including the due dates and DRIs from project plan
* **Meeting:** MPM organizes the bi-weekly 30 minute check-in call to cover milestones met and determine any blockers/at-risk action items
* **Async:** *DRIs are responsible for delivering by their due dates as many tasks are dependencies for later tasks by other teams*
* **Reporting:** MPM organizes reporting issues for each DRI to include overall metrics and more detailed drill-in by channel (one month post-launch)
* **Optimization:** MPM creates issues for optimizing the landing page, channels, etc. and assigning to relevan DRIs

## New Ideas for Marketing Campaigns or Events

- Ideas for marketing campaigns
   - We begin by asking: "Who is the target? What is the desired outcome? How does this drive results for GitLab?"
   - If you have a good idea of who the campaign is for and why it would benefit them and us, create an issue in the [Digital Marketing Programs repo](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new) using the template named *campaign-suggestion* detailing the campaign concept.
- Ideas for events
  - We are constantly on the look out for high value events, both large and small, to attend or sponsor, in order to engage with our target audiences.
  - Depending on the number and type of attendees at an event, it may be owned by Corporate Marketing, Field Marketing, or Community Relations. Please review our [events decision tree](https://docs.google.com/spreadsheets/d/1aWsmsksPfOlX1t6TeqPkh5EQXergt7qjHAjGTxU27as/edit?usp=sharing) to ensure you are directing your event inquiry to the appropriate team.
  - Direct your request to the appropriate team by creating an issue in the marketing repo and tagging someone from the proper team. For example: If your event qualifies as a Field Marketing event, create an issue in the marketing repo describing the event and rationale to attend/sponsor, and tag the field marketing manager.
- Both teams reserve the right to decline, but we love hearing your ideas! Understand that we need to fit every request into a program we’re running to meet our OKRs, as well as balance the needs of our community and our business.
- If it’s an item you can execute without much help, you’re more likely to be given a green light (e.g., a wallpaper that the design team can easily create).

Note: The Marketing Team owns content on marketing pages; do not change content or design of these pages without consulting the Manager, Digital Marketing Programs. Marketing will request help from product/UX when we need it, and work with them to ensure the timeline is reasonable.
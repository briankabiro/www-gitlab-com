describe Gitlab::Homepage::Team::Member do
  describe '.all!' do
    subject { described_class.all! }

    it 'correctly integrates with data/team.yml' do
      expect(subject).not_to be_empty
      expect(subject).to all(be_a described_class)

      expect(subject.first.username).to eq 'dzaporozhets'
    end

    it 'correctly loads report titles' do
      expect(subject.first.reports_to_title).to match('CEO')
    end

    it 'sorts live team data without errors' do
      expect { subject.sort }.not_to raise_error
    end
  end

  describe '.no_vacancies' do
    it 'removes open positions' do
      expect(described_class.no_vacancies).not_to be_empty
      expect(described_class.no_vacancies.length).to be < described_class.all!.length
    end
  end

  describe '#==' do
    it 'returns true if both slugs match' do
      member_a = described_class.new('slug' => 'a')
      member_a_two = described_class.new('slug' => 'a')

      expect(member_a).to eq(member_a_two)
    end

    it 'returns false when slugs do not match' do
      member_a = described_class.new('slug' => 'a')
      member_b = described_class.new('slug' => 'b')

      expect(member_a).not_to eq(member_b)
    end
  end

  describe '<=>' do
    let(:member_a) { described_class.new('slug' => 'a', 'start_date' => Date.new(2012, 1, 1)) }
    let(:member_b) { described_class.new('slug' => 'b', 'start_date' => Date.new(2018, 7, 1)) }
    let(:vacancy) { described_class.new('slug' => 'vacancy', 'start_date' => nil, 'type' => 'vacancy') }
    let(:vacancy_with_start_date) { described_class.new('slug' => 'vacancy_b', 'start_date' => Date.new(2018, 1, 1), 'type' => 'vacancy') }

    it 'sorts by start date' do
      members = [member_b, member_a]

      expect(members.sort).to eq([member_a, member_b])
    end

    it 'sorts vacancies last, even if they have earlier start dates' do
      members = [vacancy_with_start_date, vacancy, member_b, member_a]

      expect(members.sort).to eq([member_a, member_b, vacancy_with_start_date, vacancy])
    end
  end

  describe '#hash' do
    it 'returns the slug' do
      member_a = described_class.new('slug' => 'a')
      member_a_two = described_class.new('slug' => 'a')
      member_b = described_class.new('slug' => 'b')

      expect([member_a, member_b] - [member_a_two]).to eq([member_b])
    end
  end

  describe '#assign' do
    let(:member) do
      described_class.new('username' => 'grzesiek',
                          'projects' => { 'gitlab' => 'reviewer' })
    end

    let(:project) do
      Gitlab::Homepage::Team::Project
        .new('gitlab', name: 'GitLab')
    end

    it 'creates a new project assignment' do
      member.assign(project)

      expect(member.assignments.count).to eq 1
      expect(member.assignments.first).to be_reviewer
    end
  end

  describe '#text_role' do
    subject do
      described_class.new('username' => 'grzesiek',
                          'role' => '<a href="/job-families/engineering/developer/">Senior Developer, <strong>Verify (CI)</strong></a>')
    end

    it 'returns the role with HTML tags stripped' do
      expect(subject.text_role).to eq('Senior Developer, Verify (CI)')
    end
  end

  describe '#project_roles' do
    context 'when user has only one role in the project' do
      subject do
        described_class.new('projects' =>
                              { 'gitlab' =>
                                'maintainer backend' })
      end

      it 'returns an inverted project role hash' do
        expect(subject.project_roles['gitlab']).not_to be_empty
        expect(subject.project_roles['gitlab']).to all(be_a String)
      end
    end

    context 'when user has only one role in the project' do
      subject do
        described_class.new('projects' =>
                              { 'gitlab' =>
                                ['maintainer backend', 'owner'] })
      end

      it 'returns an inverted project role hash' do
        expect(subject.project_roles['gitlab']).not_to be_nil
        expect(subject.project_roles['gitlab']).to all(be_a String)
      end
    end
  end

  describe '#involved?' do
    subject do
      described_class.new('projects' =>
                            { 'gitlab' =>
                              ['maintainer backend', 'owner'] })
    end

    context 'when user is involved in the project' do
      it 'indicates that user is involved in the project' do
        project = double(key: 'gitlab')

        expect(subject.involved?(project)).to be true
      end
    end

    context 'when user is not involved in the project' do
      it 'indicates that user is not involved in the project' do
        project = double(key: 'gitlab-foss')

        expect(subject.involved?(project)).to be false
      end
    end
  end

  describe '#start_date_for_sort' do
    context 'when the member has a start_date' do
      subject { described_class.new('start_date' => Date.new(2019, 2, 1)) }

      it 'returns the start date' do
        expect(subject.start_date_for_sort).to eq(subject.start_date)
      end
    end

    context 'when the member does not have a start_date' do
      subject { described_class.new({}) }

      before do
        allow(Date).to receive(:today).and_return(Date.new(2018, 1, 1))
      end

      it 'returns today' do
        expect(subject.start_date_for_sort).to eq(Date.today)
      end
    end
  end

  describe '#vacancy_for_sort' do
    context 'when the member is a vacancy' do
      subject { described_class.new('type' => 'vacancy') }

      it 'returns 1' do
        expect(subject.vacancy_for_sort).to eq(1)
      end
    end

    context 'when the member is not a vacancy' do
      subject { described_class.new({}) }

      it 'returns 0' do
        expect(subject.vacancy_for_sort).to eq(0)
      end
    end
  end

  describe 'middleman compatible delegated data sources' do
    subject { described_class.new('name' => 'grzesiek') }

    context 'when data key exists' do
      it 'delegates access to data source keys' do
        expect(subject.name).to eq 'grzesiek'
      end
    end

    context 'when data key does not exist' do
      it 'returns nil without raising an error' do
        expect(subject.country).to be_nil
      end
    end
  end
end
